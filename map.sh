#!/bin/bash

if
    go build && ./syncthing-graph $@ >out.dot
then
    xdot out.dot &
    convert out.dot out.png
fi
