/*
syncthing-graph
Copyright (C) 2023  Andrea Trentini

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

/* MAIN ***********************************************************/
func main() {
	l := len(os.Args) - 1
	if l < 1 {
		//fmt.Println("Usage: syncthing-graph <config.xml> ...")
		os.Stderr.WriteString("Usage: syncthing-graph <config.xml> ...\n")
		os.Exit(1)
	}

	dedup = make(map[string]int)

	var confs []Config // roots

	// header
	fmt.Print("digraph map {\ngraph [ratio=\"1\",rankdir = \"LR\"]\n")

	// devices
	fmt.Print("\n# *** devices ***\n")
	for count, arg := range os.Args[1:] {
		fmt.Printf("\n# device: %s\n\n", arg)
		logger.Println("=== Processing file", arg, "(", count+1, "of", l, ")") // TODO: printf
		conf := UnmarshalConfig(arg)
		confs = append(confs, conf)

		// prima estraggo i device coi nomi
		for _, d := range conf.Devices {
			if deduper(d.Id) {
				//fmt.Printf("\"%s\" [shape=box]\n", d.Id) // just id
				//fmt.Printf("\"%s\" [label=\"%s\",shape=box]\n", d.Id, d.Name) // id as nodename (not shown), name as label
				fmt.Printf("\"%s\" [label=\"%s\n%s\",shape=box,style=rounded]\n", d.Id, d.Name, "") //d.Id) // id as nodename (not shown), name+id as label
			}
		}
	}

	// folders
	for _, conf := range confs {
		dotFolders(conf)
	}

	// footer
	fmt.Println("}")
}

/* VARS ***********************************************************/
var logger = log.Default()
var dedup map[string]int

// root
type Config struct {
	fileName string
	XMLName  xml.Name `xml:"configuration"`
	Folders  []Folder `xml:"folder"`
	Devices  []Device `xml:"device"`
}

type Folder struct {
	XMLName xml.Name `xml:"folder"`
	Id      string   `xml:"id,attr"`
	Label   string   `xml:"label,attr"`
	Path    string   `xml:"path,attr"`
	Type    string   `xml:"type,attr"`
	Devices []Device `xml:"device"`
}

type Device struct {
	XMLName xml.Name `xml:"device"`
	Id      string   `xml:"id,attr"`
	Name    string   `xml:"name,attr"`
}

/* FUNCs ***********************************************************/
func dotFolders(conf Config) {
	fmt.Printf("\n# folders of: %s\n\n", conf.fileName)

	// poi i folders coi link
	for _, f := range conf.Folders {
		if deduper(f.Id) {
			//fmt.Printf("\"%s\" [label=\"%s\",shape=folder]\n", f.Id, f.Label)
			fmt.Printf("\"%s\" [label=\"%s\n%s\",shape=folder]\n", f.Id, f.Label, "") //f.Id) // id+label in label
			//fmt.Printf("\"%s\"\n", f.Id)

			//////////////////////////////////////////////////////////////////
			// sendonly (default in directed graph), sendreceive, receiveonly
			/*
				ty := "label=sendonly" // sendonly
				if f.Type == "sendreceive" {
					ty = "label=sendreceive,dir=none"
				}
				if f.Type == "receiveonly" {
					ty = "label=receiveonly,dir=back"
				}
				//logger.Println("DEBUG", f.Type, ty)
			*/

			// links
			for _, d := range f.Devices {
				if deduper(f.Id + d.Id) {
					fmt.Printf("\"%s\" -> \"%s\"\n", f.Id, d.Id)
					//fmt.Printf("\"%s\" -> \"%s\" %s\n", d.Id, f.Id, ty) // just id->id
					//fmt.Printf("\"%s\" -> \"%s\" [%s]\n", f.Id, d.Id, ty)
					//fmt.Printf("\"%s\" -> \"%s\" %s [label=\"%s\"]\n", f.Id, d.Id, ty, f.Label) // la label finale NON serve
				}
			}
		}
	}
}

/* ret true if new string */
func deduper(s string) bool {
	/*
		os.Stderr.WriteString(s)
		os.Stderr.WriteString("\n")
		return true
	*/
	if dedup[s] == 0 {
		dedup[s]++
		//logger.Println(s, "non c'era")
		return true
	}
	return false
}

/* accresce la root (non male!) MA con duplicazioni */
func UnmarshalConfig(filename string) (conf Config) {
	xmlFile, err := os.Open(filename)
	if err != nil {
		logger.Println(err)
	}
	defer xmlFile.Close()
	conf.fileName = filename
	byteValue, _ := ioutil.ReadAll(xmlFile)
	xml.Unmarshal(byteValue, &conf)
	return
}
