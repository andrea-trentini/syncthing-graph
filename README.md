# syncthing-graph

Very [simple](https://en.wikipedia.org/wiki/Unix_philosophy) graph (dot format) generator for Syncthing config.xml

![example result](example.png)

It takes a list of config.xml files as parameters on the command line and then it produces (on stdout) a single dot file that can be viewed through `xdot` (on GNU/Linux).

## CAVEAT

It just works


## Example usage

Clone this repo.

Build it (`go build`).

Get all the `config.xml` files you want to process and then:

```
$ ./syncthing-graph *.xml > graph.dot
```


Then you can visualize the graph with:

```
$ xdot graph.dot
```

(I'm assuming a reasonable Linux system)

## Need other graphics formats?

Use [ImageMagick](https://imagemagick.org/index.php) (i.e., `convert` under GNU/Linux) to generate other image formats, examples:
```
$ convert graph.dot graph.png
$ convert graph.dot graph.jpg
```

(see also `map.sh`)


## How to get remote xml files?

- I have (keyed) ssh access to all the nodes of my network...

- I don't think it's a good idea to share config.xml through Syncthing itself